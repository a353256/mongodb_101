
# Tarea | Mongodb 101

Instrucciones
En esta tarea vamos a utilizar una colección de calificaciones de estudiantes, todas las respuestas deben de colocarse en un archivo llamado tarea_1.js y debe subirse a gitlab (siendo público el repositorio), utilizando comentarios, deben colocarse las preguntas, luego los comandos utilizados y de nuevo en comentarios las respuestas, al finalizar debe entregarse la liga de gitlab con la tarea.

1) Baja el archivo grades.json y en la terminal ejecuta el siguiente comando: $ mongoimport -d students -c grades < grades.json

2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?

3) Encuentra todas las calificaciones del estudiante con el id numero 4.

4) ¿Cuántos registros hay de tipo exam?

5) ¿Cuántos registros hay de tipo homework?

6) ¿Cuántos registros hay de tipo quiz?

7) Elimina todas las calificaciones del estudiante con el id numero 3

8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?

9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100

10) A qué estudiante pertenece esta calificación.

¡Suerte!

Este proyecto utiliza un contenedor de Docker para ejecutar una instancia de MongoDB

## Requisitos previos

- Docker instalado en tu sistema. Puedes descargar Docker desde [el sitio web oficial de Docker](https://www.docker.com/get-started).

## Uso

Siga estos pasos para ejecutar un contenedor de Docker con MongoDB:

1. Descargar la imagen de MongoDB desde Docker Hub:

   ```bash
   docker pull mongo
   ```

2. Crear y ejecutar el contenedor de MongoDB:

   ```bash
   docker run -d --name mi-contenedor-mongo -p 27017:27017 mongo
   ```

3. Verifica que el contenedor esté en ejecución:

   ```bash
   docker ps
   ```

4. una vez que todo funcione entramos a donde tenemos el documento de grades.json y luego ejecutamos esto
   ```bash
   sudo docker exec -ti mi-contenedor-mongo mongoimport -d students -c grades < grades.json
   ```

5. Luego ejecutamos lo siguiente para entrar al bash
   ```bash
   sudo docker exec -i mi-contenedor-mongo bash
   ```

6. Para empezar a trabajar y estando dentro del bash escribimos
   ```bash
   mongosh
   ``

7. Para empezar a trabajar ahi
   ```bash
   test>use students
   ``


## Autor

Tarea elabaroada por Miriam Fernanda Arellanes Perez 353256
Profesor Luis Antonio Ramirez Martinez
